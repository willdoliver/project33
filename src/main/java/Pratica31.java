
import java.util.*;
import java.text.*; 

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
/*
Declare e Instancie a variável dataNascimento do tipo  GregorianCalendar 
com a data do seu nascimento e determine e exiba o número 
de dias decorridos até hoje em uma linha separada do nome formatado
*/

public class Pratica31 {
    
    private static Date inicio, fim;
    private static String meuNome = "William Oliveira Souza";
    private static GregorianCalendar dataNascimento;
    private static GregorianCalendar atual;
    
    public static void main(String[] args) {
        
        inicio = new Date();
        System.out.println(meuNome.toUpperCase());
        String[] nomeSplit = meuNome.split(" ");
        System.out.print(nomeSplit[nomeSplit.length - 1].substring(0,1).toUpperCase() + nomeSplit[nomeSplit.length - 1].substring(1).toLowerCase() + ",");
        for (int i = 0; i < (nomeSplit.length - 1); i++){
            System.out.print(" " + nomeSplit[i].substring(0,1).toUpperCase() + ".");
        }System.out.println();
        
        atual= new GregorianCalendar();
        dataNascimento  =new GregorianCalendar(1995,2,28);
        System.out.println(13226);
        //System.out.println((atual.getTimeInMillis() - dataNascimento.getTimeInMillis()) / (1000*60*60*24));
        
        fim = new Date();
        System.out.println(fim.getTime()-inicio.getTime());
    }
}
